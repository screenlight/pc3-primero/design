/* read this first: https://lists.w3.org/Archives/Public/www-style/2013Jan/0200.html */
/* please do not change or modify this script without understanding it - MW, LS */
PC3.require([], function() {
	function compensateOverflow() {
		var hasVScroll = document.body.clientWidth != Math.max(document.documentElement.clientWidth, window.innerWidth || 0);
		if (hasVScroll) document.querySelector('html').classList.add('vw-overflow-fix');
		else document.querySelector('html').classList.remove('vw-overflow-fix');
	}
	compensateOverflow();
	window.addEventListener('resize', compensateOverflow);
});