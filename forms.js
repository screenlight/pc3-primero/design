PC3.require(['pc3lib/jquery-3.4.1'], function($) {
	function register() {
		$('input').each(function(index, input) {
			if (input.value) input.classList.add('has-value');
			else input.classList.remove('has-value');
			
			$(input).on('keyup', function() {
				if (input.value) input.classList.add('has-value');
				else input.classList.remove('has-value');
			});
		});
		
		$('.form-input-reset').on('click', function() {
			$(this).parent().find('.form-input-search').val('');
			if (!!this.getAttribute('auto-submit')) pc3SubmitForm(this.form, false);
		});
	}
	register();
	$(document).on('pc3/ajax/content/completed', register);
});