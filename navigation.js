PC3.define('primero/design/navigation', ['pc3lib/jquery-3.4.1'], function($) {
	
	var loadedLoginForm = false;
	var openResetForm = false;
	var searchInputHandling = false;
	
	function init(){
		// console.log('init navigation');
		
		$(document).on('click', '.overlay-trigger', function() {
			
			var overlay = $(this).data('overlay');
			var $target = $('.overlay-' + overlay);
			
			
			if ($target.length < 1) {
				console.log('Navigation: No target found');
				return;
			}
			
			if ($target.hasClass('active')) {
				closeMenu($target);
				return;
			}
			
			$('body').addClass('mod-no-scroll');
			$target.addClass('active');
			
			$target.find('a.link').on('click.close', function(){
				closeMenu($target);
			});
			
			// LOGIN
			if (!loadedLoginForm && overlay == 'login') {
				loadLoginForm();
			}
			
			// MENU
			if (!searchInputHandling && overlay == 'menu') {
				handleSearchInput($target);	
			}
		});
		
		// open login form if reset hash is provided
		if (!openResetForm) checkResetHash();
		
		
	}
	
	function handleSearchInput($target) {
		searchInputHandling = true;
		
		var $input = $target.find('.form-input-search');
		var $icon = $target.find('.search .icon.icon-close');
		
		$input.on('keyup', function(){
			var v = $(this).val();
			if (v) $icon.removeClass('mod-display-none');
			else $icon.addClass('mod-display-none');
		});
		
		$icon.on('click', function() {
			$icon.addClass('mod-display-none');
		});
	}
	
	function checkResetHash() {

		if ($.urlParam('ResetHash')) {
			
			setTimeout(function () {
				var $target = $('.overlay.login');
				
				$target.addClass('active');
				$target.find('a.link').on('click.close', function(){
					closeMenu($(this));
				});
				
				loadLoginForm('reset');
				
			}, 300);
			
		}
		
		openResetForm = true;
	}
	
	function loadLoginForm(show) {
		var id = (show && show == 'reset') ? '#ajax-pwreset-form-url' : '#ajax-login-form-url';
		var url = $(id).attr('href');
		var ajax = 'ajax-login-form';
	
		if (!url) return;
		
		PC3.require('pc3/ajax/content').requestURL(ajax, url);
		loadedLoginForm = true;
	}
	
	function closeMenu($target) {
		$target.find('a.link').off('click.close');

		setTimeout(function(){
			$target.removeClass('active');	
			$('body').removeClass('mod-no-scroll');
		});
	}
	
	$.urlParam = function(name){
		var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
		
		if (results) return results;
		else return null;
	}
	
	function updateBreadcrumbs() {
		var $newBreadcrumb = $(document).find('#navigation-breadcrumb-update > ul');
		if ($newBreadcrumb.length > 0) $('#navigation-breadcrumb > ul').replaceWith($newBreadcrumb);
	}
	
	return {
		init: init,
		updateBreadcrumbs: updateBreadcrumbs
	}
});