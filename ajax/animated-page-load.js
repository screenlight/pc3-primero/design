PC3.define('primero/design/ajax/animated-page-load', ['pc3lib/jquery-3.4.1'], function($) {
	return {
		run: function() {
			var globalAjaxContent = document.querySelector('#GlobalAjax');
			var spinnerTimeout;
			
			// remove loading-class on initial page load without ajax
			window.requestAnimationFrame(function() {
				if (globalAjaxContent) globalAjaxContent.classList.remove('loading');
			});
				
			// on each ajax-page load, add loading-class, scroll to top and show spinner
			$(globalAjaxContent).on('pc3/ajax/content/requesting', function(e) {
				if (e.target == globalAjaxContent) {
					globalAjaxContent.classList.add('loading');
					setTimeout(function() {
						window.scrollTo(0, 0);
					}, 300);
					spinnerTimeout = setTimeout(function() {
						var spinnerWrapper = document.querySelector('.ajax-spinner-wrapper.global');
						if (spinnerWrapper) spinnerWrapper.classList.add('visible');
					}, 1000);
				}
			});
			
			window.addEventListener('popstate', function() {
				$(document).trigger('default/lazyloading/trigger');
			});
			
			// on each ajax-success, remove loading-class and hide spinner
			$(globalAjaxContent).on('pc3/ajax/content/success', function(e) {
				if (e.target == globalAjaxContent) {
					window.scrollTo(0, 0);
					if (spinnerTimeout) clearTimeout(spinnerTimeout);
					var spinnerWrapper = document.querySelector('.ajax-spinner-wrapper.global');
					if (spinnerWrapper) spinnerWrapper.classList.remove('visible');
					
					window.requestAnimationFrame(function() {
						globalAjaxContent.classList.remove('loading');
					});
				}
			});
		}
	};
});