PC3.define('primero/design/ajax/link-activator', ['pc3lib/jquery-3.4.1'], function($) {
	return {
		activate: function(linkId) {
			var navigationLinksActive = document.querySelectorAll('[data-navigation-linkid].active');
			if (navigationLinksActive) {
				;[].forEach.call(navigationLinksActive, function(navigationLinkActive) {
					navigationLinkActive.classList.remove('active');
				});
			}
			
			var navigationLinks = document.querySelectorAll('[data-navigation-linkid="' + linkId + '"]');
			if (navigationLinks) {
				;[].forEach.call(navigationLinks, function(navigationLink) {
					navigationLink.classList.add('active');
				});
			}
		}
	}
});