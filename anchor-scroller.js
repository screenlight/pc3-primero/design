PC3.define('primero/design/anchor-scroller', ['pc3lib/jquery-3.4.1'], function($) {
	function getCurrentAnchor() {
		var splitted = window.location.href.split('#');
		return splitted[1];
	}
	
	function scrollToAnchor(anchor) {
		var links = document.querySelectorAll('a');
		var scrolled = false;
		;[].forEach.call(links, function(link) {
			if (scrolled) return;
			
			if (link.name == getCurrentAnchor()) {
				scrolled = true;
				
				$([document.documentElement, document.body]).animate({
					scrollTop: $(link).offset().top - 60
				}, 1000);
			}
		});
	}
	
	return {
		scrollToAnchor: scrollToAnchor
	};
});
