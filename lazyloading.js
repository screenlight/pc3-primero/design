PC3.define('primero/design/lazyloading', ['pc3lib/jquery-3.4.1'], function($) {
    var lazyloading = {};

    function load() {
        getImages();

        var scrollTop = $(document).scrollTop();
        var scrollTopVp = scrollTop + Math.max(document.documentElement.clientHeight, window.innerHeight || 0);

        ;
        [].forEach.call(lazyloading.images, function(image) {

			image.classList.add('lazyload-animated');

            var lazyloaded = image.getAttribute('lazyloaded') == 'true';
            if (!lazyloaded && scrollTop <= image.top + image.height && scrollTopVp >= image.top && image.top > 0) {
            	
				pc3.lazyload(image, null, false);
            	image.setAttribute('lazyloaded', true);
                
                window.requestAnimationFrame(function() {
                	image.classList.remove('hidden');
                });
            }
        });
    }

    function getImages() {
        lazyloading.images = document.querySelectorAll('.pc3-img-lazy-load');

        ;
        [].forEach.call(lazyloading.images, function(image) {
            if (image.getAttribute('lazyloaded') != 'true') image.classList.add('hidden');
            image.top = getPos(image).y;
            image.height = $(image).height();
        });
    }

    function getPos(el) {
        for (var lx = 0, ly = 0; el != null; lx += el.offsetLeft, ly += el.offsetTop, el = el.offsetParent);
        return {
            x: lx,
            y: ly
        };
    }

    window.addEventListener('scroll', load);
    window.addEventListener('resize', load);
    window.addEventListener('DOMContentReady', load)
    window.addEventListener('popstate', load);
    $(document).on('default/lazyloading/trigger', load);
    $(document).on('pc3/ajax/content/success', load);

    return {
        load: load
    };
});