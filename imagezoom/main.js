PC3.define('primero/design/imagezoom', ['primero/design/imagezoom/core', 'primero/design/imagezoom/ui', 'pc3lib/jquery-3.4.1'], function(ImageZoom, ImageZoomUi, $) {

	$(document).on('pc3/ajax/content/success', function(e) {
		if (e.target.id == 'GlobalAjax') init();
	});

    function init() {
        var overlayElement = document.querySelectorAll('.image-zoom-overlay')[0];

        var imageZoomInit = function($galleryElements) {
        	
            var items = [];
            var openImageZoom = function($wrap, position, galleryElements) {
                var options = {
                    index: position,
                    getThumbBoundsFn: function(i) {
                    	var thumbnail = (items[i].rectEl) ? items[i].rectEl : items[i].el;
                        var pageYScroll = window.pageYOffset || document.documentElement.scrollTop;
						var rect = thumbnail.getBoundingClientRect();
						
                        return {
                            x: rect.left,
                            y: rect.top + pageYScroll,
                            w: rect.width
                        };
                    }
                };
                var ga = new ImageZoom.core(overlayElement, ImageZoomUi.ui, items, options);
                ga.init();
                ga.listen('/default/utilities/pageTransition/start', function(container, direction) {
                    var $tmpContainer = $(container);
                    $tmpContainer.addClass('container-transition');
                    $tmpContainer.on('webkitTransitionEnd transitionend msTransitionEnd oTransitionEnd', function() {
                        $tmpContainer.removeClass('container-transition');
                    });
                });
                // hide caption on zoom
                ga.listen('/default/utilities/imageZoom/zoom-in', function() {
                    $(ga.scrollWrap).find('.image-zoom-caption').css({
                        opacity: 0
                    })
                });
                ga.listen('/default/utilities/imageZoom/zoom-out', function() {
                    $(ga.scrollWrap).find('.image-zoom-caption').css({
                        opacity: 1
                    })
                });
                ga.listen('/default/utilities/imageZoom/onBeforeSlideTransition', function(e, isDown, preventObj, ee) {
                    if (isDown && $(this.currItem.container).find('.video-js').length > 0) {
                        $(window).trigger("/default/utilities/video/stop-all");
                    }
                });
            };

            $galleryElements.each(function(index) {
                var $wrapper = $(this);
                $wrapper.attr('data-zoom-image', 0);
                $wrapper.addToGallery = $wrapper.attr('data-zoom-image-gallery');
                $wrapper.zoomImageSource = $wrapper.attr('data-zoom-image-url');
                $wrapper.origImageSource = $wrapper.attr('src');
                $wrapper.el = $wrapper.get(0);

                if ($wrapper.hasClass('video-player')) {
                    /*
                     * video mode:
                     * no zoom/pan is possible
                     * scaling is allowed
                     * height and width can be a ratio eg 16:9 but also 1920:1080
                     */
                    var ratio = $wrapper.attr('data-ratio'),
                        ratioArr = ratio.split(":");
                    $wrapper.ratioH = ratioArr[0];
                    $wrapper.ratioW = ratioArr[1];
                    $wrapper.zoomImageSource = $wrapper.attr('poster');
                    $wrapper.origImageSource = $wrapper.zoomImageSource;

                    var $oldVideoTag = $wrapper.parent().find('.video-js'); // video tag
                    var $oldVideoSourceTags = $wrapper.parent().find('source'); // video tag

                    // clone is buggy in FF and IE -> create new Player from scratch
                    $videoTag = $('<video/>', {
                        'class': $oldVideoTag.attr('class'),
                        'controls': $oldVideoTag.attr('controls'),
                        'poster': $oldVideoTag.attr('poster'),
                        'aria-label': $oldVideoTag.attr('arial-label'),
                        'role': $oldVideoTag.attr('role'),
                        'autoplay': $oldVideoTag.attr('autoplay'),
                        'width': $oldVideoTag.attr('width'),
                        'height': $oldVideoTag.attr('height'),
                        'data-setup': $oldVideoTag.attr('data-setup'),
                        'src': $oldVideoTag.attr('src')
                    });
                    $oldVideoSourceTags.each(function() {
                        $videoSourceTag = $('<source/>', {
                            'src': $oldVideoSourceTags.attr('src'),
                            'type': $oldVideoSourceTags.attr('type')
                        });
                        $videoSourceTag.appendTo($videoTag);
                    });

                    var $clone = $wrapper.parent().parent().clone(); // clone environment
                    $clone.children().children().remove(); // clean it up with initialized video tag
                    $videoTag.appendTo($clone.children().first()); // apppend video tag to clone
                    $wrapper.el = $clone.get(0);
                    $wrapper.rectEl = $wrapper.parent().get(0); // element for rect calc
                    $wrapper.video = true;
                    var imageCaption = $wrapper.closest('.video-wrapper').find('.video-caption').html(); // inner html
                    $wrapper.title = "";
                    if ($(imageCaption).addClass("enh-invert").get(0)) {
                        $wrapper.title = $(imageCaption).addClass("enh-invert mod-align-text-center").get(0).outerHTML;
                    }
                } else {
                    $wrapper.rectEl = $wrapper.parent().get(0); // element for rect calc
                }

                if (!$wrapper.zoomImageSource) {
                    $wrapper.zoomImageSource = $wrapper.origImageSource;
                }
                var tmp = new Image();
                tmp.src = $wrapper.zoomImageSource;
                $(tmp).one('load', function() {
                    var caption = "";
                    var imageCaption = $wrapper.closest('.image-wrapper').find('.image-caption').html();
                    if ($(imageCaption).addClass("enh-invert").get(0)) {
                        caption = $(imageCaption).addClass("enh-invert mod-align-text-center").get(0).outerHTML;
                    }
                    var item = {
                        container: $wrapper.parent(),
                        src: $wrapper.zoomImageSource,
                        msrc: $wrapper.origImageSource,
                        w: ($wrapper.video) ? $wrapper.ratioH : tmp.width,
                        h: ($wrapper.video) ? $wrapper.ratioW : tmp.height,
                        el: $wrapper.el,
                        rectEl: $wrapper.rectEl,
                        title: ($wrapper.video) ? $wrapper.title : caption,
                        mode: ($wrapper.video) ? 'video' : 'image'
                    };
                    items[index] = item;
                });
                if ($wrapper.video) {
                    $wrapper.parent().find('.image-zoom-icon').on('touchend mouseup', function() {
                        openImageZoom($(this).parent(), index, $galleryElements);
                    });
                } else {
                	var startX = 0;
                	$wrapper.parent().on('touchstart', function(e) {
                		startX = e.originalEvent.touches[0].screenX;
                	});
                    $wrapper.parent().on('touchend mouseup', function(e) {
                    	if (e && e.originalEvent && e.originalEvent.changedTouches && e.originalEvent.changedTouches[0].screenX) {
                    		if (Math.abs(startX - e.originalEvent.changedTouches[0].screenX) > 100) return;
                    	}
                        openImageZoom($(this), index, $galleryElements);
                    });
                }
            });
        }

        if (!overlayElement) {
            return;
        }

        // Gallery Items with Group
        var $selection = $('[data-zoom-image-gallery="1"]');
        $selection.groups = [];
        $selection.each(function() {
            var group;
            $selection.groups[$(this).attr('data-zoom-image-group')] = 1;
        });

        for (var key in $selection.groups) {
            imageZoomInit($('[data-zoom-image-gallery="1"][data-zoom-image-group=' + key + ']'));
        }

        // Single Image Items - only images
        var $gallerySingleItems = $('[data-zoom-image="1"]:not([data-zoom-image-gallery="1"])');
        $gallerySingleItems.each(function(index) {
            //imageZoomInit($(this));
        });
    }

    return {
        'init': init
    }

});