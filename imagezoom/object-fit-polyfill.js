PC3.require(['pc3lib/jquery-3.4.1'], function($) {
	window.slObjectFit = {};
	window.slObjectFit.initialize = function() {};

	if (getInternetExplorerVersion() == -1) return;
	
	$(document).on('pc3/ajax/content/success', function(e) {
		if (e.target.id == 'GlobalAjax') initialize();
	});
	
	var debouncer;
	window.addEventListener('resize', function() {
		clearTimeout(debouncer);
		debouncer = setTimeout(reset, 50);
	});
	
	initialize();
	
	window.slObjectFit = {};
	window.slObjectFit.initialize = initialize;

	function initialize() {
		
		setTimeout(function() {
		
			var imageEs = document.querySelectorAll('[data-sl-object-fit]');
			;[].forEach.call(imageEs, function(imageE) {
				// prevent double activation of script per image
				
				var method = imageE.getAttribute('data-sl-object-fit');
				if (imageE.classList.contains('d-cover')) method = 'cover';
		
				imageE.style.flex = '0 0 auto';
				imageE.style.objectFit = 'initial';
				
				// get dimensions of image without object-fit = output dimensions of image		
				var initialDimensions = imageE.getBoundingClientRect();
		
				// create div, which serves as a relative wrapper with the initial dimensions of the image
				if (!imageE.parentNode.getAttribute('data-sl-object-fit-wrapper')) {
					var divE = document.createElement('div');
					divE.setAttribute('data-sl-object-fit-wrapper', true);
					divE.style.position = 'relative';
					divE.style.width = initialDimensions.width + 'px';
					divE.style.height = initialDimensions.height + 'px';
					divE.style.overflow = 'hidden';
					imageE.parentNode.insertBefore(divE,imageE);
					
					// append the original image to the wrapper, also removes it from its original parent
					divE.appendChild(imageE);
				}
				
				// position the image absolute in the new wrapper
				imageE.style.position = 'absolute';
				imageE.style.width = 'auto';
				imageE.style.height = 'auto';
		
				// take the original image dimensions
				var originalDimensions = imageE.getBoundingClientRect();
				
				// center the image in the wrapper
				imageE.style.top = '50%';
				imageE.style.left = '50%';
				imageE.style.transform = 'translateX(-50%) translateY(-50%)';
				
				// set container width
				imageE.style.width = Math.ceil(initialDimensions.width + 1) + 'px';
				
				var finalDimensions = imageE.getBoundingClientRect();
				
				if (method == 'contain') {
					// check if image fits
					if (finalDimensions.height > initialDimensions.height) {
						// if not, set height to container height
						imageE.style.width = 'auto';
						imageE.style.height = Math.ceil(initialDimensions.height + 1) + 'px';
					}
				}
				
				if (method == 'cover') {
					// check if image fits
					if (finalDimensions.height <= initialDimensions.height) {
						// if not, set height to container height
						imageE.style.width = 'auto';
						imageE.style.height = Math.ceil(initialDimensions.height + 1) + 'px';
					}
				}
			});
			
		}, 100);
	}
	
	function reset() {
		var imageEs = document.querySelectorAll('[data-sl-object-fit]');
		;[].forEach.call(imageEs, function(imageE) {
			if (!imageE.parentNode.getAttribute('data-sl-object-fit-wrapper')) return;
			
			imageE.removeAttribute('style');
			
			var wrapperE = imageE.parentNode;
			var imageEClone = imageE.cloneNode(true);
			wrapperE.parentNode.insertBefore(imageEClone, wrapperE);
			
			imageE.parentNode.removeChild(imageE);
			wrapperE.parentNode.removeChild(wrapperE);
			
		});
		
		initialize();
	}

	function getInternetExplorerVersion() {
	    var rV = -1; // Return value assumes failure.

	    if (navigator.appName == 'Microsoft Internet Explorer' || navigator.appName == 'Netscape') {
	        var uA = navigator.userAgent;
	        var rE = new RegExp("MSIE ([0-9]{1,}[\.0-9]{0,})");

	        if (rE.exec(uA) != null) {
	            rV = parseFloat(RegExp.$1);
	        }
	        /*check for IE 11*/
	        else if (!!navigator.userAgent.match(/Trident.*rv\:11\./)) {
	            rV = 11;
	        }
	    }
	    return rV;
	}
});
